# API - START WARS PLANETS

Project that provides access to the planets of the Star Wars franchise

# Actuator
- Application data: http://localhost:8080/api/star-wars/actuator/info
- Application health: http://localhost:8080/api/star-wars/actuator/health/ 
- Application information: http://localhost:8080/api/star-wars/actuator/info/ 
- Other urls: http://localhost:8080/api/star-wars/actuator/


# Documentation
### Swagger
- Urls access: http://localhost:8080/api/star-wars/swagger-ui.html
