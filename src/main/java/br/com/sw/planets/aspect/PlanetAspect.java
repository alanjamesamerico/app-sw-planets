package br.com.sw.planets.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.sw.planets.service.StarWarsAPIService;
import br.com.sw.planets.service.dto.PlanetDocumentDTO;
import lombok.extern.log4j.Log4j2;


@Log4j2
@Aspect
@Component
public class PlanetAspect {
	
	@Autowired
	private StarWarsAPIService apiService;
	
	@Around("@annotation(br.com.sw.planets.annotation.ProcessCreation)")
	public Object processPlanetName(ProceedingJoinPoint joinPoint) throws Throwable {
		
		log.info("Intercepting the request via Aspect");
		
		final PlanetDocumentDTO planetDTO = (PlanetDocumentDTO) joinPoint.getArgs()[0];
		
		if (this.apiService.processesExistencePlanet(planetDTO.getName())) {
			return joinPoint.proceed(joinPoint.getArgs());
		}
		return null;
	}
}
