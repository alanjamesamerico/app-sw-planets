package br.com.sw.planets.repository;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import br.com.sw.planets.domain.document.PlanetDocument;

/**
 * Interface that provides access to the MongoDB database
 *
 */
@EnableScan
public interface PlanetRepository extends CrudRepository <PlanetDocument, String> {

	/**
	 * Searches a planet by name ignoring uppercase and lowercase letters
	 * 
	 */
	PlanetDocument findByName(String name);
}
