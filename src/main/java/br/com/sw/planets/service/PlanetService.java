package br.com.sw.planets.service;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.sw.planets.domain.document.PlanetDocument;
import br.com.sw.planets.excepton.PlanetDeletionException;
import br.com.sw.planets.excepton.PlanetNotFoundException;
import br.com.sw.planets.excepton.PlanetNotFoundException.Key;
import br.com.sw.planets.excepton.PlanetPersistenceException;
import br.com.sw.planets.repository.PlanetRepository;
import br.com.sw.planets.service.dto.PlanetDocumentDTO;
import br.com.sw.planets.web.rest.PlanetRestController;
import lombok.extern.log4j.Log4j2;

/**
 * Class responsible for providing services to the class {@link PlanetRestController}
 * 
 */
@Log4j2
@Service
public class PlanetService {
	
	@Autowired
	private PlanetRepository repository;
	
	@Autowired
	private StarWarsAPIService starWarsAPIService;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private ObjectMapper mapper;
	
	public PlanetDocumentDTO savePlanet(PlanetDocumentDTO planetDTO) {
		
		log.info(checkNotNull(env.getProperty("planet.service.info.create.running")));
		
		PlanetDocument planet = this.mapper.convertValue(planetDTO, PlanetDocument.class);
		planet.setAppearances(
			this.starWarsAPIService.getAppearancesFilmByName(planetDTO.getName())
		);
		
		PlanetDocument planetSaved = this.repository.save(planet);
		if(planetSaved == null) {
			throw new PlanetPersistenceException();
		}
		log.info(env.getProperty("planet.service.info.create.success"));
		
		return this.mapper.convertValue(planetSaved, PlanetDocumentDTO.class);
	}
	
	public List<PlanetDocumentDTO> listAllPlanets() {
		
		log.info(checkNotNull(env.getProperty("planet.service.info.findAll.running")));
		
		List<PlanetDocumentDTO> planets = new ArrayList<PlanetDocumentDTO>();
		
		this.repository.findAll().forEach(planet -> {
			planets.add(this.mapper.convertValue(planet, PlanetDocumentDTO.class));
		});
		
		if(planets.isEmpty()) {
			throw new PlanetNotFoundException();
		}
		log.info(env.getProperty("planet.service.info.findAll.success"));
		
		return planets;
	}
	
	public PlanetDocumentDTO findPlanetById(String id) {
		
		log.info(checkNotNull(env.getProperty("planet.service.info.findId.running")));
		Optional<PlanetDocument> planetSearched = this.repository.findById(id);
		
		if(planetSearched.isEmpty()) {
			throw new PlanetNotFoundException(id, Key.ID);
		}
		log.info(env.getProperty("planet.service.info.findId.success"));
		
		return this.mapper.convertValue(
				planetSearched.get(), PlanetDocumentDTO.class);
	}
	
	public void deletePlanetById(@Valid String id) {
		this.repository.findById(id).map(record -> {
				this.repository.deleteById(id);
				log.info(env.getProperty("planet.service.info.findId.success"));
				return true;
			}
		).orElseThrow(() -> {
			return new PlanetDeletionException(id);
		});
	}
	
	public PlanetDocumentDTO findPlanetByName(@Valid String name) {
		log.info(
			checkNotNull(
					env.getProperty("planet.service.info.findName.running")
			));
		PlanetDocument planet = this.repository.findByName(name);
		if(planet == null) {
			throw new PlanetNotFoundException(name, Key.NAME);
		}
		
		return this.mapper.convertValue(planet, PlanetDocumentDTO.class);
	}
	
	public void updatePlanet(String id, PlanetDocumentDTO planetDto) { 
		log.info(checkNotNull(env.getProperty("planet.service.info.updating").concat(planetDto.toString())));
		
		if (this.repository.existsById(id)) {
			
			PlanetDocument planetToUpdate = this.mapper.convertValue(planetDto, PlanetDocument.class);
			planetToUpdate.setId(id);
			planetToUpdate.setAppearances(
				this.starWarsAPIService.getAppearancesFilmByName(planetDto.getName())
			);
			this.starWarsAPIService.processesExistencePlanet(planetDto.getName());
			
			this.repository.save(planetToUpdate);
			
			log.info(checkNotNull(env.getProperty("planet.service.info.update")));
		} else {
			throw new PlanetNotFoundException(planetDto.getId(), Key.ID);
		}
	}
}
