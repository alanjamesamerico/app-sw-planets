package br.com.sw.planets.service;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import br.com.sw.planets.excepton.StarWarsAPINotFoundException;
import br.com.sw.planets.service.dto.StarWarsAPIResultPlanetDTO;
import br.com.sw.planets.web.client.StarWarsAPIClient;

/**
 * Class responsible for providing services for the official Star Wars API 
 * 
 * @see <a href="https://swapi.dev">API Star Wars</a>
 *
 */
@Service
public class StarWarsAPIService {

	/**
	 * The variable that gives access to the official Star Wars API
	 */
	@Autowired
	private StarWarsAPIClient apiClient;
	
	@Autowired
	private Environment env;
	
	public Integer getAppearancesFilmByName(String name) {
		StarWarsAPIResultPlanetDTO searchResult = this.apiClient.getPlanetByName(name);
		if(StringUtils.equals(searchResult.getCount(), "0")) {
			return Integer.valueOf(0);
		}
		return Optional.of(searchResult.getResults().get(0)).get().getFilms().size();
	}
	
	public boolean processesExistencePlanet(String name) {
		StarWarsAPIResultPlanetDTO response = this.apiClient.getPlanetByName(name);
		if (StringUtils.equals(response.getCount(), "1") && response.getResults().size() == 1) {
			return true;
		}
		throw new StarWarsAPINotFoundException(
				this.env.getProperty("exception.api-sw.not-found.warn"));
	}
}
