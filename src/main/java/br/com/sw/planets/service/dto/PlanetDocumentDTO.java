package br.com.sw.planets.service.dto;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlanetDocumentDTO {
	
	@JsonIgnore
	private String id;		
	
	@NotEmpty(message = "Required name field")
	private String name;

	@NotEmpty(message = "Required climate field")
	private String climate;

	@NotEmpty(message = "Required terrain field")
	private String terrain;
	
	@JsonIgnore
	private Integer appearances;
}
