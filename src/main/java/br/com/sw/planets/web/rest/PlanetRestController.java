package br.com.sw.planets.web.rest;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.sw.planets.annotation.ProcessCreation;
import br.com.sw.planets.service.PlanetService;
import br.com.sw.planets.service.dto.PlanetDocumentDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

/**
 * Class responsible for providing access to the Star Wars Planet API services
 *
 */
@Log4j2
@RestController
@RequestMapping(path = "/planets")
@Api(tags = { "Planet Rest Controller" })
public class PlanetRestController {

	@Autowired
	private PlanetService service;

	@Autowired
	private Environment env;
	
	@PostMapping
	@ProcessCreation
	@ApiOperation(value = "Service that allows the creation of a planet")
	@ApiResponses(value = { 
			@ApiResponse(code = 201, message = "Planet created"),
			@ApiResponse(code = 202, message = "Access accepted") })
	public ResponseEntity<?> save(@RequestBody 
								  @Valid final PlanetDocumentDTO planetDTO) {
		log.info(checkNotNull((env.getProperty("planet.rest.info.save.access"))));
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.savePlanet(planetDTO));
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public void update(@PathVariable("id") String id, @RequestBody PlanetDocumentDTO planetDto) {
		log.info(checkNotNull(env.getProperty("planet.rest.info.update.access")));
		this.service.updatePlanet(id, planetDto);
	}
	
	@GetMapping
	@ApiOperation(value = "Service that allows you to search for all the planets")
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "Found planets"),
		@ApiResponse(code = 404, message = "No planet was found") })
	public ResponseEntity<?> listAll() {
		log.info(env.getProperty("planet.rest.info.findAll.access"));
		return ResponseEntity.ok().body(this.service.listAllPlanets());
	}
	
	@GetMapping("/{id}")
	@ApiOperation(value = "Service that allows you to search for a planet by id")
	public ResponseEntity<?> findById(@PathVariable @Valid final String id) {
		log.info(checkNotNull(env.getProperty("planet.rest.info.findId.access")));
		return ResponseEntity.ok().body(this.service.findPlanetById(id));
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Service that allows you to delete a planet by id")
	public ResponseEntity<?> deleteById(@PathVariable @Valid final String id) {
		log.info(checkNotNull(env.getProperty("planet.rest.info.delete.access")));
		this.service.deletePlanetById(id);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/name/{name}")
	@ApiOperation(value = "Service that allows you to search for a planet by name")
	public ResponseEntity<?> findByName(@PathVariable @Valid final String name) {
		log.info(checkNotNull(env.getProperty("planet.rest.info.findName.access")));
		return ResponseEntity.ok().body(this.service.findPlanetByName(name));
	}
}
