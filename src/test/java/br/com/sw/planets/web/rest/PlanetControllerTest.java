package br.com.sw.planets.web.rest;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;

import br.com.sw.planets.aspect.PlanetAspect;
import br.com.sw.planets.excepton.PlanetNotFoundException;
import br.com.sw.planets.excepton.PlanetNotFoundException.Key;
import br.com.sw.planets.repository.PlanetRepository;
import br.com.sw.planets.service.PlanetService;
import br.com.sw.planets.service.dto.PlanetDocumentDTO;
import br.com.sw.planets.utils.ModelUtils;
import br.com.sw.planets.utils.RequestUtils;
import io.restassured.http.ContentType;

@WebMvcTest(PlanetRestController.class)
class PlanetControllerTest {

	@Autowired
	private PlanetRestController plancetController;
	
	@MockBean
	private PlanetService service;
	
	@MockBean
	private PlanetAspect aspect;
	
	@Autowired
	private WebApplicationContext context;
	
	@MockBean
	private PlanetRepository repository;
	
	private PlanetDocumentDTO planetDTO;
	
	@BeforeEach
	void setUp() throws Exception {
		standaloneSetup(this.plancetController);
		this.planetDTO = ModelUtils.getPlanetDTOInstance();
	}

	@Test
	public void mustReturnCreated201_WhenCreateOnePlanet() {
		when(this.service.savePlanet(this.planetDTO)).
		thenCallRealMethod();
		
		given()
			.webAppContextSetup(context)
				.auth()
					.with(httpBasic(RequestUtils.USER, RequestUtils.PASS))
			.and()
			 	.header(RequestUtils.CONTENT_TYPE, RequestUtils.APPLICATION_JSON)
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
			.and()
			.body(new Gson().toJson(this.planetDTO))
		.when()
			.post(RequestUtils.URI)
		.then()
			.statusCode(HttpStatus.CREATED.value());
	}
	
	@Test
	public void mustRreturnSuccess200_WhenFindOnePlanetById() throws Exception {
		when(this.service.findPlanetById(ModelUtils.ID)).
		thenReturn(this.planetDTO);
		
		given()
			.webAppContextSetup(context)
				.auth()
					.with(httpBasic(RequestUtils.USER, RequestUtils.PASS))
			.accept(ContentType.JSON)
		.when()
			.get(RequestUtils.URI + ModelUtils.ID)
		.then()
			.statusCode(HttpStatus.OK.value());
	}

	@Test
	public void mustRreturnNotFound404_WhenFindOnePlanetById() throws Exception {
		String id = "777f6b76-4403-4cea-8354-3a8e9462a333";
		when(this.service.findPlanetById(id)).
		thenThrow(new PlanetNotFoundException(id, Key.ID));
		
		given()
			.webAppContextSetup(context)
				.auth()
					.with(httpBasic(RequestUtils.USER, RequestUtils.PASS))
            .header(RequestUtils.CONTENT_TYPE, RequestUtils.APPLICATION_JSON)
		.when()
			.get(RequestUtils.URI + id)
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
}
