package br.com.sw.planets.utils;

import java.util.Arrays;
import java.util.List;

import br.com.sw.planets.domain.document.PlanetDocument;
import br.com.sw.planets.service.dto.PlanetDocumentDTO;
import br.com.sw.planets.service.dto.StarWarsAPIPlanetDTO;
import br.com.sw.planets.service.dto.StarWarsAPIResultPlanetDTO;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class ModelUtils {
	
	public static final String ID = "114f6b76-4403-4cea-8354-3a8e9462a991";
	
	public static PlanetDocumentDTO getPlanetDTOInstance() {
		return PlanetDocumentDTO.builder()
								.id(ID)
								.name("Nal Hutta")
								.terrain("urban, oceans, swamps, bogs")
								.climate("temperate")
								.build();
	}
	
	public static PlanetDocument getPlanetDocumentInstance() {
		return PlanetDocument.builder()
							 .id(ID)
							 .name("Nal Hutta")
							 .terrain("urban, oceans, swamps, bogs")
							 .climate("temperate")
							 .build();
	}
	
	public static StarWarsAPIResultPlanetDTO getAPIResult() {
		
		List<StarWarsAPIPlanetDTO> results = Arrays.asList(
				StarWarsAPIPlanetDTO.builder()
									.name("Nal Hutta")
									.climate("hot")
									.terrain("jungles, oceans, urban, swamps	")
									.build()
		);
		
		return StarWarsAPIResultPlanetDTO.builder()
				.count("")
				.next("")
				.previous("")
				.results(results)
				.build();
	}
}
