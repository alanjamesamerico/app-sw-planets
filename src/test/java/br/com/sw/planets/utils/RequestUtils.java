package br.com.sw.planets.utils;

public class RequestUtils {
	
	public static final String URI = "/planets/";
	
	public static final String USER = "sw";
	
	public static final String PASS = "sw777";
	
	public static final String CONTENT_TYPE = "Content-Type";
	
	public static final String 	APPLICATION_JSON = "application/json";
}
