package br.com.sw.planets.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.sw.planets.domain.document.PlanetDocument;
import br.com.sw.planets.excepton.PlanetPersistenceException;
import br.com.sw.planets.repository.PlanetRepository;
import br.com.sw.planets.service.dto.PlanetDocumentDTO;
import br.com.sw.planets.utils.ModelUtils;
import br.com.sw.planets.web.client.StarWarsAPIClient;

@ExtendWith(MockitoExtension.class)
class PlanetServiceTest {

	@InjectMocks
	private PlanetService service;
	
	@Mock
	private PlanetRepository repository;
	
	@Mock
	private Environment env;
	
	@Mock
	private StarWarsAPIService swApiService;
	
	@Mock
	private ObjectMapper mapper;
	
	@Mock
	private StarWarsAPIClient apiClient;
	
	private PlanetDocument planetDocument;
	
	private PlanetDocumentDTO planetDTO;
	
	@BeforeEach
	void setUp() {
		this.planetDocument = ModelUtils.getPlanetDocumentInstance();
		this.planetDTO = ModelUtils.getPlanetDTOInstance();
	}
	
	@Test
	void mustReturnPlanetById() {
		String logInfoCreateRunning = "Searching for a planet by id...";
		when(this.env.getProperty("planet.service.info.findId.running")).
		thenReturn(logInfoCreateRunning);
		
		when(this.mapper.convertValue(this.planetDocument, PlanetDocumentDTO.class)).
		thenReturn(this.planetDTO);
		
		when(this.repository.findById(ModelUtils.ID)).
		thenReturn(Optional.of(this.planetDocument));
		
		PlanetDocumentDTO foundPlanet = this.service.findPlanetById(ModelUtils.ID);
		
		assertNotNull(foundPlanet);
	}
	
	@Test
	void shouldThrowPlanetPersistenceException() {
		String logInfoCreateRunning = "Planet being saved. . .";
		when(this.env.getProperty("planet.service.info.create.running")).
		thenReturn(logInfoCreateRunning);
		
		when(this.mapper.convertValue(this.planetDTO, PlanetDocument.class)).
		thenReturn(this.planetDocument);
		
		when(this.repository.save(this.planetDocument)).
		thenReturn(null);
		
		assertThrows(PlanetPersistenceException.class, () -> this.service.savePlanet(this.planetDTO));
	}
	
	@Test
	void mustReturnSavedPlanet() {
		String logInfoCreateRunning = "Planet being saved. . .";
		when(this.env.getProperty("planet.service.info.create.running")).
		thenReturn(logInfoCreateRunning);
		
		when(this.swApiService.getAppearancesFilmByName("Nal Hutta")).
		thenReturn(Integer.valueOf(1));
		
		when(this.mapper.convertValue(this.planetDTO, PlanetDocument.class)).
		thenReturn(this.planetDocument);
		
		when(this.repository.save(this.planetDocument)).
		thenReturn(this.planetDocument);
		
		String logInfoCreateSuccess = "Planet created";
		when(this.env.getProperty("planet.service.info.create.success")).
		thenReturn(logInfoCreateSuccess);
		
		when(this.mapper.convertValue(planetDocument, PlanetDocumentDTO.class)).
		thenReturn(this.planetDTO);
		
		PlanetDocumentDTO planetSaved = this.service.savePlanet(this.planetDTO);
		
		assertNotNull(planetSaved);
	}
}
